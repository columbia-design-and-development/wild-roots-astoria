<?php
/**
 * Template Name: Booking Page
 *
 */

get_header(); ?>
	<div id="primary" class="row-fluid">
		<div id="content" role="main" class="span8 offset2">
			<div class = "iframe-container">
				<iframe src = "https://www.schedulicity.com/scheduling/wrmakr/services"></iframe>
			</div>
		</div><!-- #content .site-content -->
	</div><!-- #primary .content-area -->
<?php get_footer(); ?>