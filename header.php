<?php
	/*-----------------------------------------------------------------------------------*/
	/* Template: header
	/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>
	<?php bloginfo('name'); ?> | 
	<?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
</title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>

</head>

<body 
	<?php body_class("pg-" . preg_replace("/[^a-z]+/", "", strtolower(get_the_title()))); ?>
>

<header id="masthead" class="site-header">
	<div class = "hero-static">
		<div class = "hero-info-container">
			<div class = "menu-open-button header-button">Menu</div>
			<a href = "<?php echo get_page_link(16) ?>", class = "call-to-action header-button">Book Now</a>
			<h1 class="title"><?php wp_title(''); ?></h1>
			<i class = "fa fa-angle-up" id = "scroll1"></i>
		</div>		
		
	</div>
	<div class = "nav-menu">
		<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		<div class = "menu-close-button header-button">X</div>
	</div>
</header><!-- #masthead .site-header -->

<main class="main-fluid"><!-- start the page containter -->
