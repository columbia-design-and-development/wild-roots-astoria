<?php 
/**
 * 	Template Name: Accordian Bodyworks
*/

$block1 = get_field('swedish_comfort');
$block2 = get_field('breadwinner');
$block3 = get_field('matriarch/patriarch');
$block4 = get_field('the_whole_tribe');
$block5 = get_field('relaxibis');
$block6 = get_field('salty_dog');
$block7 = get_field('everyday_athlete');

get_header(); ?>
<div id="primary" class="row-fluid">
	<div id="content" role="main" class="span12">
		<a href="https://www.schedulicity.com/scheduling/WRMAKR" class = "booking-button" title="Online scheduling" target="_blank"><img src="//cdn.schedulicity.com/images/schedulenow_lt_yellow7_lg.png" alt="Online scheduling" title="Online scheduling" border="0" /></a>
        <div class = "accordian">
            <input type="checkbox" id="bodyworktab1" name="tabs">
            <label for="bodyworktab1">
				<h1>Swedish Comfort</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab1">
                    <?php echo $block1 ?>
                </div>
            </div>
            <input type="checkbox" id="bodyworktab2" name="tabs">
            <label for="bodyworktab2">
				<h1>Breadwinner</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab2">
                    <?php echo $block2 ?>
                </div>
            </div>
            <input type="checkbox" id="bodyworktab3" name="tabs">
            <label for="bodyworktab3">
                <h1>Matriarch/Patriarch</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab3">
                    <?php echo $block3 ?>
                </div>
            </div>
            <input type="checkbox" id="bodyworktab4" name="tabs">
            <label for="bodyworktab4">
                <h1>The Whole Tribe</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab4">
                    <?php echo $block4 ?>
                </div>
            </div>
            <input type="checkbox" id="bodyworktab5" name="tabs">
            <label for="bodyworktab5">
                <h1>Relaxibis</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab5">
                    <?php echo $block5 ?>
                </div>
            </div>
            <input type="checkbox" id="bodyworktab6" name="tabs">
            <label for="bodyworktab6">
                <h1>Salty Dog</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab6">
                    <?php echo $block6 ?>
                </div>
            </div>
            <input type="checkbox" id="bodyworktab7" name="tabs">
            <label for="bodyworktab7">
                <h1>Everyday Athlete</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "bodyworktab7">
                    <?php echo $block7 ?>
                </div>
            </div>
        </div>

    </div>
</div>


<?php get_footer(); ?>