<?php
	/*-----------------------------------------------------------------------------------*/
	/* Template: footer
	/*-----------------------------------------------------------------------------------*/
?>

</main><!-- / end page container -->

<footer class="site-footer">
	<hr>
	<div class = "social-icon-container">
		<?php get_sidebar();?>
		<h1>elisabeth.pietila@gmail.com</h1>
	</div>
	<hr>
	<div class = "footer-nav-container">
		<?php wp_nav_menu( array( 'theme_location' => 'primary' ) );?>
	</div>
	<div class="signature-container">
		
		<p>Powered by Elliott</p>
		
	</div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->

<script>
	jQuery(document).ready(function() {
		jQuery("#scroll1").click(function() {
			jQuery('html, body').animate({scrollTop: jQuery("#content").offset().top}, 700);
		});
		jQuery('.menu-open-button').click(function(){
			jQuery('.nav-menu').animate({right: '0'}, 700);
			jQuery('.hero-info-container').animate({left: '100vw'}, 700);
		});
		jQuery('.menu-close-button').click(function(){
			jQuery('.nav-menu').animate({right: '100vw'}, 700, function () { jQuery(this).removeAttr('style'); });
			jQuery('.hero-info-container').animate({left: '0'}, 700, function () { jQuery(this).removeAttr('style'); });
		});
    });
</script>

<?php wp_footer(); ?>

</body>
</html>
