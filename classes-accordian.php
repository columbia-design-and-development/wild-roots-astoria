<?php 
/**
 * 	Template Name: Accordian Classes
*/

$block1 = get_field('foga');
$block2 = get_field('wild_barre');
$block3 = get_field('pound_fitness');
$block4 = get_field('mamalates');

get_header(); ?>
<div id="primary" class="row-fluid">
	<div id="content" role="main" class="span12">
		<a href="https://www.schedulicity.com/scheduling/WRMAKR" class = "booking-button" title="Online scheduling" target="_blank"><img src="//cdn.schedulicity.com/images/schedulenow_lt_yellow7_lg.png" alt="Online scheduling" title="Online scheduling" border="0" /></a>
        <div class = "accordian">
            <input type="checkbox" id="classtab1" name="tabs">
            <label for="classtab1">
				<h1>FOGA</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "classtab1">
                    <?php echo $block1 ?>
                </div>
            </div>
            <input type="checkbox" id="classtab2" name="tabs">
            <label for="classtab2">
				<h1>Wild Barre</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "classtab2">
                    <?php echo $block2 ?>
                </div>
            </div>
            <input type="checkbox" id="classtab3" name="tabs">
            <label for="classtab3">
                <h1>Pound Fitness</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "classtab3">
                    <?php echo $block3 ?>
                </div>
            </div>
            <input type="checkbox" id="classtab4" name="tabs">
            <label for="classtab4">
                <h1>Mamalates</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "classtab4">
                    <?php echo $block4 ?>
                </div>
            </div>
        </div>

    </div>
</div>


<?php get_footer(); ?>