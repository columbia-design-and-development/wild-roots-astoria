<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
<?php bloginfo( 'description' ); ?>
<?php bloginfo( 'title' ) ?>
<?php get_sidebar(); ?>
<?php get_template_part('home-header');?>
<?php echo get_stylesheet_directory_uri(); ?>
